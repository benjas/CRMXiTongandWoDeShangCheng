# CRM系统and我的商城（全开源+大量代码注释+环境配图+免费技术支持[仅这个项目的问题]）

++++++++++++++++++

1. 应大家需求：把小程序的后台管理数据/仅cengtos部署版本，实现代码在博客，超简单（利用flask-admin开发出来[可增删改]，能够显示数据[而，也是小白级，如果要更加完善需要各位的努力]）。
2. 新增功能销售顾客板块、会议室、商城购物车(参考之前做的一个小程序项目做的)，代码已放置个人网盘做独立板块，有兴趣的朋友可以加我QQ。
3. 有人提到关于框架问题，对于小程序而言，后端更推荐使用sanic框架，但是小程序没有linux版，sanic在window方面的支持又比较差，如果想要更好的性能，可以使用sanic后端+uni-app前端
4. 博客更新了用uwsgi+supervisor来代替runserver的方法，亲们一定要看更新呐

++++++++++++++++++

### Description（分支下载flask[window10后端]/flask_centos[部署上线后端]/flask_ubuntu[Ubuntu后端]，微信小程序[前端]即可）
全开源、超简单的flask后端+微信小程序，里面大部分代码功能都有注释（小白级），旨在帮助更多人更快的开发flask+微信小程序。员工管理系统及我的个性化商城，超轻量级入门flaks和微信小程序

### 关于配置环境、知识等可以移步到我的博客（有人联系到我问我配置跟报错，惊喜，特此做了这个博客）https://www.cnblogs.com/Benjas/articles/11037450.html

## 项目截图
![![输入图片说明](https://images.gitee.com/uploads/images/2019/0701/212027_896f331b_4999522.png "新增功能.png")](https://images.gitee.com/uploads/images/2019/0701/212025_e5aa019c_4999522.png "屏幕截图.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/163832_094ac89d_4999522.png "1.png")](https://images.gitee.com/uploads/images/2019/0618/101546_4d1b32cf_4999522.png "主页.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/163849_8d5120af_4999522.png "公司.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/163857_3cd7f8bc_4999522.png "商城.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/163910_acd76727_4999522.png "编辑.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0617/163916_e83241c9_4999522.png "注册.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2019/0618/100433_bf6a25df_4999522.png "python开发部分代码.png")](https://images.gitee.com/uploads/images/2019/0618/100432_a0b0da08_4999522.png "python开发部分代码.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2019/0619/191831_44af8036_4999522.png "后台数据展示.png")](https://images.gitee.com/uploads/images/2019/0619/191830_d17191cd_4999522.png "后台数据展示.png")

### 部署环境要求
    Centos7+
    Nginx
    python3+
    pymysql/MySQL5.1+
建议使用环境：Centos7/Nginx + python3.6 + MySQL5.7

## 有任何问题请联系我的QQ909336740

#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)